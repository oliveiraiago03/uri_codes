while True:
    try:
        N = int(input())
        if N>=1001 and N<=9999:
            print(N-1)
    except EOFError:
        break

#detalhe para o tratamento do erro, pois sem ele o compilador gera esse erro e não aceita a solução
#https://discuss.codechef.com/t/taking-input-python/10808/4
#link para entender melhor o motivo