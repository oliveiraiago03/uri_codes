entrada = input().split()
#cria uma lista com os valores escritos em uma linha, o separador é o espaço
valores = [int(i) for i in entrada]
# ordena a lista valores de forma ascendente
valores.sort()
# o *antes do nome da lista imprime os valores da lista
print(*valores, sep='\n')
print()
print(*entrada, sep='\n')