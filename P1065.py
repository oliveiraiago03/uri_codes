#Ideia
#Criar um programa que leia 5 valores e se o valor lido for par em módulo
#Jogar numa lista e depois mostrar o len da lista

#cria uma variavel contadora auxiliar
cont = 0
lista = []
while cont < 5:
    valor = int(input())
    if valor%2 ==0:
        lista.append(valor)
    cont+=1

quant = len(lista)

print("{} valores pares".format(quant))

